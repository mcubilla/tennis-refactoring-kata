
public class TennisGame1 implements TennisGame {

    private int player1Score = 0;
    private int player2Score = 0;
    private final String player1Name;
    private final String player2Name;

    public TennisGame1(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public void wonPoint(String playerName) {
        if (playerName.equals(player1Name)){
            player1Score += 1;
        }else{
            player2Score += 1;
        }
    }

    public String getScore() {
        if (player1Score == player2Score) return getTiedScoreMessage();
        if (player1Score >=4 || player2Score >=4) return getScoreHigherThanFourMessage();
        return getScoreMessage(player1Score) + "-" + getScoreMessage(player2Score);
    }

    public void resetScore(){
        this.player1Score = 0;
        this.player2Score = 0;
    }

    private String getTiedScoreMessage(){
        switch (player1Score){
            case 0:
                return "Love-All";
            case 1:
                return "Fifteen-All";
            case 2:
                return "Thirty-All";
            default:
                return "Deuce";
        }
    }

    private String getScoreHigherThanFourMessage(){
        int scoreDifference = player1Score - player2Score;
        if (scoreDifference==1) return "Advantage player1";
        if (scoreDifference ==-1) return "Advantage player2";
        if (scoreDifference>=2) return "Win for player1";
        return "Win for player2";
    }

    private String getScoreMessage(int Score){
        switch(Score)
        {
            case 0:
                return "Love";
            case 1:
                return "Fifteen";
            case 2:
                return "Thirty";
            default:
                return "Forty";
        }
    }
}
