
public class TennisGame3 implements TennisGame {

    private final int WIN_OR_ADVANTAGE_TREESHOLD_POINTS = 4;
    private final int WIN_OR_ADVANTAGE_TREESHOLD_TOTAL_POINTS = 6;
    private final String[] SCORE_MESSAGE_VALUES = new String[]{"Love", "Fifteen", "Thirty", "Forty"};

    private int player2Points;
    private int player1Points;
    private final String player1Name;
    private final String player2Name;

    public TennisGame3(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore() {
        if (isPlayerUnderWinOrAdvantageTreeshold(player1Points)
                && isPlayerUnderWinOrAdvantageTreeshold(player2Points)
                &&  !isGameOnWinOrAdvantageStatus()){
            return getDefaultMessage();
        }

        if (player1Points == player2Points) {
            return "Deuce";
        }

        return getWinningOrAdvantageMessage();
    }

    private Boolean isPlayerUnderWinOrAdvantageTreeshold(int playerPoints){
        return playerPoints < WIN_OR_ADVANTAGE_TREESHOLD_POINTS;
    }

    private Boolean isGameOnWinOrAdvantageStatus(){
        return player1Points + player2Points == WIN_OR_ADVANTAGE_TREESHOLD_TOTAL_POINTS;
    }

    private String getDefaultMessage(){

        String scoreMessage = SCORE_MESSAGE_VALUES[player1Points];

        if(player1Points == player2Points) {
            return scoreMessage + "-All";
        }

        return scoreMessage + "-" + SCORE_MESSAGE_VALUES[player2Points];
    }

    private String getWinningOrAdvantageMessage(){
        String playerOnTop = player1Points > player2Points ? player1Name : player2Name;

        if(isSomeoneOnAdvantage()){
            return "Advantage " + playerOnTop;
        }

        return "Win for " + playerOnTop;
    }

    private Boolean isSomeoneOnAdvantage(){
        return player2Points - player1Points == 1 || player1Points - player2Points == 1;
    }

    public void wonPoint(String playerName) {
        if (playerName.equals(player1Name)) {
            this.player1Points += 1;
        } else {
            this.player2Points += 1;
        }
    }
    
    public void resetScore(){
        this.player1Points = 0;
        this.player2Points = 0;
    }

}
